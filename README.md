# AmpBox

The AmpBox is a little speaker amplifier that I designed and built out of parts
I had lying around.

The DIYLC layout is missing offboard components (volume pot, power switch and
LED). Refer to the schematic for those.
